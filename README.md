# How to manage Minio

## Setting up the Minio client

All management of Minio is done with the Minio client `mc`.
You can the sources and installation instructions for `mc` on [github](https://github.com/minio/mc).

Once you have installed the client you need to configure it.

```bash
mc alias set <ALIAS> <YOURS3-ENDPOINT> <YOUR-ACCESS-KEY> <YOUR-SECRET-KEY>
```

- ALIAS: the name you want to give to your Minio server with associated access key and secret key
- YOURS3-ENDPOINT: the URL of your Minio server
- YOUR-ACCESS-KEY
- YOUR-SECRET-KEY

Note that if you have multiple access keys and secret keys you will need to assign different aliases. For example

```bash
mc alias set myminio-user1 https://myminio.example.com user1-access-key user1-secret-key
mc alias set myminio-user2 https://myminio.example.com user2-access-key user2-secret-key
mc alias set other-minio https://otherminio.com some-access-key  some-secret-key
```

You can also run `mc` interactively:

```bash
❯ mc alias set myserver https://minio.mydomain.com
Enter Access Key: access-key
Enter Secret Key: 
Added `myserver` successfully.
```

Listing the buckets:

```bash
❯ mc ls myserver
[2020-12-01 11:41:54 CET]     0B bucket1/
[2020-12-01 11:41:27 CET]     0B bucket2/
```

## About the play minio server

Each Minio client has a play Minio server configured. The examples below use the play Minio server.

You can access the play minio server at https://play.minio.io

:exclamation: 

## Basic commands

Creating a bucket on the `play` minio server with name `example-bucket`.

```bash
❯ mc mb play/example-bucket
Bucket created successfully `play/example-bucket`.
```

Listing buckets on the play server that you have access to (this depends on your access key and secret key):

```bash
❯ mc ls play
[2020-12-04 22:50:34 CET]     0B 00test/
[2020-12-05 10:16:15 CET]     0B 1f0e5272-a559-40ed-9a8a-81b6db3cf83f/
[2020-12-05 10:07:55 CET]     0B 2063b651-92a3-4a20-a4a5-03a96e7c5a89/
[2020-12-05 09:11:14 CET]     0B 3c044c0e-2010-48ff-9dc1-c41eeebfb723/
[2020-12-05 08:36:23 CET]     0B 637f4d17-df73-4898-8a9e-a48ab15621f4/
[2020-12-05 09:11:13 CET]     0B 639c06e3-65dc-4309-bb79-d9746fdf5b6f/
```

Deleting the bucket `example-bucket` on the `play` Minio server:

```bash
❯ mc rb play/example-bucket
Removed `play/example-bucket` successfully.
```

## Managing policies 

In order to set permissions on which user can access which buckets you need to configure policies on the Minio server.

By default the Minio server providers several policies out of the box: `writeonly`, `readonly` and `readwrite`.  Unfortunately these are very insecure.

Look up the list of policies on the server:
```bash
❯ mc admin policy list myserver
diagnostics         
readonly            
readwrite           
writeonly  
```

To create our own policy we start by creating a `mypolicy.json` file containing the policy statements. The format follows s3 policies and get be generated directly from [AWS policy generator](https://awspolicygen.s3.amazonaws.com/policygen.html).

What follows is a workflow where we create a policy that allows a user to only work inside his own bucket. We are working against the play Minio server.

First we create the full-access-on-username.json file that contains our policy.

Then we upload the policy file `full-access-on-username.json` to the play Minio server and give it the name `full-access-on-username`.

```bash
❯ mc admin policy add play full-access-on-username full-access-on-username.json
Added policy `full-access-on-username` successfully.
```

Next, we create a new user `newuser-example` (this is the access_key) with secret_key `newuser-example-password` on the play Minio server

```bash
❯ mc admin user add play newuser-example newuser-example-password
Added user `newuser-example` successfully.
```

Now we associate the `full-access-on-username` policy with the `newuser-example` user:

```bash
❯ mc admin policy set play full-access-on-username user=newuser-example
Policy full-access-on-username is set on user `newuser-example`
```

What is still missing is we need to create the bucket as the admin user:

```bash
❯ mc mb play/newuser-example
Bucket created successfully `play/newuser-example`.
```

Next, we configure an alias `newuser` so we can connect as the `newuser-example` user to the play Minio server:

```bash
❯ mc alias set newuser https://play.minio.io newuser-example newuser-example-password
Added `newuser` successfully.
```

We can now upload files to our bucket as the newuser-example user:

```bash
❯ mc mirror . play/newuser-example
...pecific-bucket.json:  638 B / 638 B ┃▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓┃ 1.14 KiB/s 0s❯ 
```

Let us try to list the contents of the bucket:

```bash
❯ mc ls newuser/newuser-example
mc: <ERROR> Unable to list folder. Access Denied.
```

This is correct as we haven not yet created a bucket `newuser-example`. So let us create it with the newuser credentials:

```bash
❯ mc mb newuser/newuser-example
Bucket created successfully `newuser/newuser-example`.
❯ mc ls newuser
[2020-12-05 14:53:32 CET]     0B newuser-example/
```

We can now upload files too. Here we mirror the current directory to the bucket `newuser-example` with the newuser credentials.

```bash
❯ mc mirror --overwrite . newuser/newuser-example
...pecific-bucket.json:  665 B / 665 B ┃▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓┃ 939 B/s 0s ~/Vcs/minio-policies    
```
Writing to another bucket fails:

```bash
❯ mc mirror --overwrite . newuser/otherbucket
mc: <ERROR> Unable to create bucket at `newuser/otherbucket/somedir/`. Access Denied.
```

The actions listed in `full-access-on-username.json` have one issue:
```
"Action": [
  "s3:*"
],
```
This allows all actions on the bucket to be done. Even deleting buckets and objects!

In certain situations it is better to create a policy and a user that can only upload data. For example a device that needs to upload log files. This way you can allow a device to only send data to the minio server but not delete it.

You can find such policy in the [upload-only-username.json](upload-only-username.json) file.


```bash
❯ mc admin policy add play upload-only-username upload-only-username.json
Added policy `upload-only-username` successfully.
```

## Using groups to make policy management easier

It is possible to assign a policy to a group of users. This makes it easier to change the policy of all users belonging to that group. Otherwise you need to update the policy of each user.

Then when creating a user you only need to assign that user to a group. Also, when creating your group you need to list the users
 
In the following example we:
* create 2 users (user1 and user2)
* add user1 to the group upload-only
* assign the upload-only-username policy to the upload-only group
* add user2 to the upload-only group

All the commands are done with the play alias (which has admin permissions).

```bash
❯ mc admin user add play user1 user1-password
Added user `user1` successfully.
❯ mc admin user add play user2 user2-password
Added user `user2` successfully.
❯ mc admin group add play upload-only user1
Added members {user1} to group upload-only successfully.
❯ mc admin policy set play upload-only-username group=upload-only
Policy upload-only-username is set on group `upload-only`
❯ mc admin group add play upload-only user2
Added members {user2} to group upload-only successfully.
```
Now we can add user1 and user2 aliases:

```bash
❯ mc alias set user1 https://play.minio.io user1 user1-password
Added `user1` successfully.
❯ mc alias set user2 https://play.minio.io user2 user2-password
Added `user2` successfully.
```

Let us create our buckets as the admin user:
```bash
❯ mc mb play/user1
Bucket created successfully `play/user1`.
❯ mc mb play/user2
Bucket created successfully `play/user2`.
```

You can verify the policy of a user with:

```bash
❯ mc admin user info play user1
AccessKey: user1
Status: enabled
PolicyName: user
MemberOf: upload-only
```

Let us now verify user1 can put files in his bucket, and not in the bucket of user2:

```bash
❯ mc mv foo user1/user1
foo:                     0 B / ? ┃░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▓┃❯ mc mv foo user1/user2
mc: <ERROR> Failed to copy `foo`. Insufficient permissions to access this file `https://play.minio.io/user2/foo`
```

## Further references

- [AWS S3 reference](https://docs.aws.amazon.com/service-authorization/latest/reference/list_amazons3.html)
